<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts_messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contact_id')->index();
            $table->unsignedInteger('user_id_to')->index();
            $table->unsignedInteger('user_id_from')->index();
            $table->text('message')->nullable();
            $table->timestamp('seen_at')->nullable();

            $table->timestamps();

            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('user_id_to')->references('id')->on('users');
            $table->foreign('user_id_from')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts_messages');
    }
}
