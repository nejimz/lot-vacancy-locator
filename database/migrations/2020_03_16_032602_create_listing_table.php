<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('listing_type_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->tinyInteger('status')->nullable();

            $table->decimal('lot_area', 10, 2)->nullable();
            $table->decimal('floor_area', 10, 2)->nullable();
            $table->decimal('floor', 10, 2)->nullable();
            $table->decimal('bedrooms', 10, 2)->nullable();
            $table->decimal('bathrooms', 10, 2)->nullable();

            $table->string('address');
            $table->string('city');
            $table->string('subdivision');
            $table->text('description');

            $table->timestamps();

            $table->foreign('listing_type_id')->references('id')->on('listing_type');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing');
    }
}
