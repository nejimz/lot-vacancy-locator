@extends('layout.master')

@section('page-title', 'Broker - ' . $broker->first_name . ' ' . $broker->last_name)

@section('content')
<style type="text/css">
    .fa-star-color {
        color: #f7c42d!important;
    }
</style>
<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-4">
            
        <h4>Broker</h4>
        <img class="thumbnail" src="{{ (!is_null($broker->image))? asset($broker->image) : 'https://placehold.it/450x300'}}" width="250px">
        <p class="subheader">
            Name : {{ $broker->first_name . ' ' . $broker->last_name }}<br>
            Agency Name : {{ $broker->agency_name }}<br>
            Member Since : {{ $broker->created_at->format('F d, Y') }}<br>
            Ratings : {!! $broker->rating !!}
        </p>
        @if(Sentinel::check())
            @if(App\BrokerRating::whereUserId(Sentinel::getUser()->id)->count() <= 0)
                @include('partials.error-messages')
                <form action="{{ route('brokers.rate', ['broker_id'=>$broker->id]) }}" method="post">
                    @csrf()
                    <select name="rate">
                        <option value="">-- Select --</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <button class="button" type="submit">Rate</button>
                </form>
            @endif
        @endif
    </div>

    <div class="cell large-7">
        <h4>Properties</h4>
        <div class="callout secondary">
            <div class="grid-x">
                @foreach($rows as $row)
                <div class="cell large-6 text-center">
                    <a href="{{ route('property.show', $row->id) }}">
                        @if(is_null($row->images->where('primary', '=', 1)->first()))
                        <img class="thumbnail" src="https://placehold.it/450x300" width="250px">
                        @else
                        <img class="thumbnail" src="{{ asset($row->images->where('primary', '=', 1)->first()->location) }}">
                        @endif
                    </a>
                    <p class="subheader">
                        <a href="{{ route('property.show', $row->id) }}">{{ $row->subdivision }}</a><br>
                        <i class="fa fa-home"></i> <i>{{ $row->type->name }} ({{ $row->lot_area }} sqm)</i><br>
                        <i class="fa fa-map-marker"></i> {{ $row->address . ', ' . $row->city }}
                    </p>
                    <br>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</div>

@endsection