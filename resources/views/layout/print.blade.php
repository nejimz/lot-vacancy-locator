<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>@yield('page-title') | Lot Vacancy Locator</title>
	<meta name="description" content="Buy plumbing supply online wholesale, as well as tools, sprinklers and HVAC supply. Save on plumbing with the home improvement experts at PlumbersStock.com.">
	<meta name="keywords" content="online plumbing supply">
	<meta name="robots" content="NOINDEX,NOFOLLOW">
	<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
	<link rel="apple-touch-icon" href="{{ asset('apple-touch-icon-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('apple-touch-icon-precomposed.png') }}">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app-custom.css') }}">
	<style type="text/css">
		#realEstateMenu,
		#realEstateMenu ul,
		#realEstateMenu div
		{
			/*background-color: #0C0F14;*/
		}
		#realEstateMenu .menu-text-logo
		{
			/*color: #ffffff;*/
			font-weight: bold;
		}
		/*#realEstateMenu ul li a,
		#realEstateMenu ul li a:link
		{
			color: #ffffff!important;
		}
		#realEstateMenu ul li a:hover
		{
			color: #7D8897!important;
		}*/
		#message-notif {
			border-radius: 13px;
			font-size: 11px;
			padding: 4px 7px 4px 7px;
		}
	</style>
</head>
<body>
	@yield('content')
	
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
	<script type="text/javascript">
		$(document).foundation();
		if (Foundation.MediaQuery.is('small only')) {
			$('.site-badge .cell').css('text-align', 'center');
		}
	</script>
	@yield('scripts')
</body>
</html>

