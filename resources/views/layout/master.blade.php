<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>@yield('page-title') | Lot Vacancy Locator</title>
	<meta name="description" content="Buy plumbing supply online wholesale, as well as tools, sprinklers and HVAC supply. Save on plumbing with the home improvement experts at PlumbersStock.com.">
	<meta name="keywords" content="online plumbing supply">
	<meta name="robots" content="NOINDEX,NOFOLLOW">
	<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
	<link rel="apple-touch-icon" href="{{ asset('apple-touch-icon-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('apple-touch-icon-precomposed.png') }}">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app-custom.css') }}">
	<style type="text/css">
		#realEstateMenu,
		#realEstateMenu ul,
		#realEstateMenu div
		{
			/*background-color: #0C0F14;*/
		}
		#realEstateMenu .menu-text-logo
		{
			/*color: #ffffff;*/
			font-weight: bold;
		}
		/*#realEstateMenu ul li a,
		#realEstateMenu ul li a:link
		{
			color: #ffffff!important;
		}
		#realEstateMenu ul li a:hover
		{
			color: #7D8897!important;
		}*/
		#message-notif {
			border-radius: 13px;
			font-size: 11px;
			padding: 4px 7px 4px 7px;
		}
	</style>
</head>
<body>
	<!-- Navigation -->
	<div class="title-bar show-for-small-only" data-responsive-toggle="realEstateMenu">
		<button class="menu-icon" type="button" data-toggle></button>
		<div class="title-bar-title">Menu</div>
	</div>

	<div class="top-bar" id="realEstateMenu">
		<div class="top-bar-left">
			<ul class="menu">
				<li class="menu-text-logo"><a href="{{ route('index') }}"><i class="fa fa-home fa-lg"></i> Home</a></li>
				<li><a href="{{ route('property.index') }}"><i class="fa fa-building"></i>Properties</a></li>
				<li><a href="{{ route('brokers.index') }}"><i class="fa fa-users"></i>Broker's</a></li>
			</ul>
		</div>
		<div class="top-bar-right">
			<ul class="menu dropdown" data-dropdown-menu>
				@if(Sentinel::check())
					<li>
						<a href="{{ route('cart.index') }}"><i class="fa fa-star fa-lg"></i>&nbsp;List</a>
					</li>
					<li>
						<a href="{{ route('inbox.index') }}">
							<?php
							$sentinel_user_id = Sentinel::getUser()->id;
							/*$msg_count = App\Contacts::select('id')->whereUserId($sentinel_user_id)
							->whereHas('message', function($query) use ($sentinel_user_id){
								$query->where('user_id_to', $sentinel_user_id)->whereNull('seen_at');
							})->toSql();*/
							$msg_count = App\ContactsMessages::where('user_id_to', $sentinel_user_id)->count();
							#dd($msg_count);
							if ($msg_count > 0) {
								echo '<span id="message-notif" class="label alert">'.$msg_count.'</span> ';
							}
							?>
							 <i class="fa fa-inbox fa-lg"></i>&nbsp;Inbox
						</a>
					</li>
					<li>
						<a href="javascript::void(0)">
							<?php
							$notification = '';
							$reserved_count = App\ListingOwner::leftJoin('listing', 'listing_owners.listing_id', '=', 'listing.id')
							->whereIn('listing.status', [0, 1])
							->whereNull('listing_owners.seen_at')
							->where('listing_owners.user_id', $sentinel_user_id)->count();
							if ($reserved_count > 0) {
								$notification = '<span id="message-notif" class="label alert">'.$reserved_count.'</span> ';
								echo '<span id="message-notif" class="label alert">'.$reserved_count.'</span> ';
							}
							?>
							<i class="fa fa-user fa-lg"></i>&nbsp;My Account
						</a>
					    <ul class="menu dropdown" data-dropdown-menu>
					    	
					    	<li><a href="{{ route('viewed.notification') }}">
					    		{!! $notification !!}<i class="fa fa-bell"></i> Nofication</a>
					    	</li>
					      	<li>
					      		<a href="{{ route('profile.index') }}"><i class="fa fa-book"></i> Profile</a>
					      	</li>
					    	@if(Sentinel::getUser()->is_broker == 1)
					      	<li>
					      		<a href="{{ route('listing.index') }}"> <i class="fa fa-list"></i> Listing</a>
					      		<ul class="menu">
					      			<li>
					      				<a href="{{ route('listing.create') }}"><i class="fa fa-plus"></i> New</a>
					      			</li>
					      		</ul>
					      	</li>
					      	<li>
					      		<a href="javascript:void(0)"> <i class="fa fa-file"></i> Reports</a>
					      		<ul class="menu">
					      			<li>
					      				<a href="{{ route('reports.listing') }}"><i class="fa fa-list"></i> Listing</a>
					      			</li>
					      		</ul>
					      	</li>
					      	@endif
					    	@if(!is_null(Sentinel::getUser()->is_admin))
					      	<li>
					      		<a href="javascript::void(0)"><i class="fa fa-cogs"></i> Admin</a>
					      		<ul class="menu">
					      			<li>
					      				<a href="{{ route('admin.users.index') }}"><i class="fa fa-users"></i> Users</a>
					      			</li>
					      			<li>
					      				<a href="{{ route('admin.activity.index') }}"><i class="fa fa-graph"></i> Users Activity Log</a>
					      			</li>
					      		</ul>
					      	</li>
					    	@endif
							<li>
								<a href="{{ route('listing.viewed') }}"><i class="fa fa-log"></i> Activity Log</a>
							</li>
							<li>
								<a href="{{ route('login.logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
							</li>
					    </ul>
					</li>
				@else
					<li><a class="button hollow" href="{{ route('login.index') }}">Login</a></li>
				@endif
			</ul>
		</div>
	</div>
	<!-- /Navigation -->
	<div class="container">

		<div class="grid-x">
			<div class="cell large-12">
				@yield('content')
			</div>
		</div>
	</div>
	
	<div class="container">
		<hr>
		<p class="text-center">&copy; Copyright Lot Vacancy Locator 2020</p>
	</div>

	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
	<script type="text/javascript">
		$(document).foundation();
		if (Foundation.MediaQuery.is('small only')) {
			$('.site-badge .cell').css('text-align', 'center');
		}
	</script>
	@yield('scripts')
</body>
</html>

