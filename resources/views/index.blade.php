@extends('layout.master-full')

@section('page-title', 'Home')

@section('content')
<style type="text/css">
    #realEstateMenu {

    }
    p.lead {
        margin-bottom: 4px!important;
    }
    .thumbnail {
        width: 250px!important;
        height: 250px!important;
    }
    html, body {
        height: 100%; 
    }
    .bg {
        background-image: url('images/plan-and-manage-hero.jpg');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: top;
        width: 100%;
        height: 100%;
        padding-top: 20%;
    }
    @media screen and (max-width: 39.9375em) {
        #find-a-property {
            padding-top: 400px;
        }
        .bg-banner {
            font-size: 36px!important;
            padding-bottom: 0!important;
            padding-top: 0!important;
        }
    }
    @media screen and (min-width: 40em) and (max-width: 63.9375em) {
        #find-a-property {
            padding-top: 400px;
        }
        #featured-prop {
            margin-top: 100px;
        }
        .bg-banner {
            font-size: 36px!important;
            padding-bottom: 0!important;
            padding-top: 0!important;
        }
    }
    .bg-banner {
        position: absolute;
        font-size: 64px;
        font-weight: bold;
        font-stretch: extra-expanded;
        color: #fff;
        top: 100px;
        text-align: center;
        width: 100%;
    }
</style>
<div class="bg">
    <!-- <h1 class="bg-banner text-center">REAL ESTATE PROPERTY LOCATOR </h1> -->
    <div id="find-a-property" class="grid-x">
        <div class="cell large-2 small-12"></div>
        <div class="cell large-3 small-12">
            <div class="callout">
                <h3 class="text-center margin-top-custom-50">Find a property</h3>
                <div class="grid-x margin-top-custom-20">
                    <div class="cell large-1"></div>
                    <div class="cell auto small-12">
                        <form action="{{ route('property.index') }}" method="get">
                            <select name="type">
                                <option value="">All</option>
                                <option value="1">Lot</option>
                                <option value="2">House & Lot</option>
                            </select>
                            <input type="text" name="search" placeholder="Search Location, Lot, Bedrooms, Floor">
                            <button class="button" type="submit"><i class="fa fa-search"></i> Search</button>
                        </form>
                    </div>
                    <div class="cell large-1"></div>
                </div>
            </div>
            <div class="cell auto"></div>
        </div>
    </div>
</div>

<div id="featured-prop" class="container margin-top-custom-50">
    <h3 class="text-center">Featured Properties</h3>
    <div class="grid-x grid-padding-x">
        @foreach($properties as $property)
        <div class="cell large-3 small-12">
            <div class="callout">
                <?php $img = $property->images->where('primary', 1)->first(); ?>
                <p class="text-center">
                    @if(is_null($img))
                    <img class="thumbnail" src="https://placehold.it/250x370">
                    @else
                    <img class="thumbnail" src="{{ $img->location }}">
                    @endif
                </p>
                <p class="lead text-center"><a href="{{ route('property.show', $property->id) }}">{{ $property->subdivision }}</a></p>
                <p class="subheader text-center">
                    <i class="fa fa-map-marker"></i> {{ $property->address . ', ' . $property->city }}<br>
                </p>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection