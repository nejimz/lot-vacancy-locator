@extends('layout.master')

@section('page-title', 'Viewed Property')

@section('content')
<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-12">
        <h3>Viewed Property</h3>
        <table width="100%">
            <thead>
                <tr>
                    <th width="30%">Subdivision</th>
                    <th width="30%">Address</th>
                    <th width="20%" class="text-center">Status</th>
                    <th width="20%">Seen At</th>
                </tr>
            </thead>
            <tbody>
            @foreach($rows as $row)
                <tr>
                    <td><a href="{{ route('property.show', $row->id) }}" title="Edit">{{ $row->listing->subdivision }}</a></td>
                    <td>{{ $row->listing->address }}</td>
                    <td class="text-center">{{ ucwords($row->listing->list_status) }}</td>
                    <td>{{ $row->created_at->format('M. d, Y H:i A') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $rows->appends(['search'=>''])->links('vendor.pagination.default') !!}
    </div>

</div>

@endsection