@extends('layout.master')

@section('page-title', 'Listing Form')

@section('content')

<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-12">
        <h3>New Listing</h3>
        @include('partials.error-messages')
        <form action="{{ $route }}" method="post">

            <div class="grid-x grid-padding-x">
                <div class="cell large-2">
                    <label>
                        Property Type
                        <select name="listing_type_id" id="listing_type_id">
                            <option value="">-- Select --</option>
                            @foreach($types as $type)
                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
                <div class="cell large-2 lot-items">
                    <label>
                        Lot Area
                        <input type="text" name="lot_area" id="lot_area" value="{{ $lot_area }}">
                    </label>
                </div>
                <div class="cell large-2 house-and-lot-items" style="display: none;">
                    <label>
                        Floor Area
                        <input type="text" name="floor_area" id="floor_area" value="{{ $floor_area }}" disabled="disabled">
                    </label>
                </div>
                <div class="cell large-2 house-and-lot-items" style="display: none;">
                    <label>
                        Floor
                        <input type="number" name="floor" id="floor" value="{{ $floor }}" disabled="disabled">
                    </label>
                </div>

                <div class="cell large-2 house-and-lot-items" style="display: none;">
                    <label>
                        Bedrooms
                        <input type="number" name="bedrooms" id="bedrooms" value="{{ $bedrooms }}" disabled="disabled">
                    </label>
                </div>
                <div class="cell large-2 house-and-lot-items" style="display: none;">
                    <label>
                        Bathrooms
                        <input type="number" name="bathrooms" id="bathrooms" value="{{ $bathrooms }}" disabled="disabled">
                    </label>
                </div>
            </div>

            <label>
                Unit Address
                <input type="text" name="title" id="title" value="{{ $title }}">
                <input type="hidden" name="title_search_id" id="title-search-id" value="0">
            </label>

            <label>
                Price
                <input type="number" name="price" id="price" value="{{ $price }}">
            </label>

            <label>
                Subdivision
                <input type="text" name="subdivision" id="subdivision" value="{{ $subdivision }}">
            </label>

            <label>
                Address
                <input type="text" name="address" id="address" value="{{ $address }}">
            </label>

            <label>
                City
                <input type="text" name="city" id="city" value="{{ $city }}">
            </label>

            <label>
                Description
                <textarea name="description" id="description" rows="5">{{ $description }}</textarea>
            </label>
            
            <div class="grid-x">
                <div class="cell auto"></div>
                <div class="cell large-2">
                    @if(isset($id))
                        @method('PUT')
                        <button class="button success expanded margin-top-custom-40" type="submit">Update</button>
                    @else
                        <button class="button success expanded margin-top-custom-40" type="submit">Create</button>
                    @endif
                    @csrf()
                    <a class="button expanded" href="{{ route('listing.index') }}">Back</a>
                </div>
                <div class="cell auto"></div>
            </div>
        </form>
    </div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
        var xhr_search = null;
        var xhr_search_select = null;
    <?php
    if ($listing_type_id == 1) {
        echo '$(".house-and-lot-items").hide();';
        echo '$(".house-and-lot-items input").attr("disabled", true);';
    } elseif ($listing_type_id == 2) {
        echo '$(".house-and-lot-items").show();';
        echo '$(".house-and-lot-items input").attr("disabled", false);';
    }
    ?>
    $('#listing_type_id').val('{{ $listing_type_id }}');

    $(function(){
        $('#listing_type_id').change(function(){
            var value = $(this).val();
            if (value == 1) {
                $('.house-and-lot-items').hide();
                $(".house-and-lot-items input").attr("disabled", true);
            } else if (value == 2) {
                $('.house-and-lot-items').show();
                $(".house-and-lot-items input").attr("disabled", false);
            } else {
                $('.house-and-lot-items ').hide();
            }
        });


        $("#title").autocomplete({
            source: function(request, response){
                xhr_search = $.ajax({
                    type : 'get',
                    url : '{{ route("listing.unit-address-search") }}',
                    cache : false,
                    dataType : "json",
                    data : "keyword=" + request.term,
                    async : false, 
                    beforeSend: function(xhr){
                        if (xhr_search != null)
                        {
                            xhr_search.abort();
                        }
                    }
                }).done(function(result_set) {
                    console.log(result_set);
                    response($.map(result_set, function (key, value) {
                        return {
                            label: key,
                            value: value
                        };
                    }));
                }).fail(function(jqXHR, textStatus) {

                });
            },
            minLength: 2,
            select: function( event, ui ) {

                xhr_search_select = $.ajax({
                    type : 'get',
                    url : '{{ route("listing.unit-address-select") }}',
                    cache : false,
                    dataType : "json",
                    data : "id=" + ui.item.value,
                    async : false, 
                    beforeSend: function(xhr){
                        if (xhr_search_select != null)
                        {
                            xhr_search_select.abort();
                        }
                    }
                }).done(function(result_set) {
                    $('#listing_type_id').val(result_set['listing_type_id']);
                    $('#lot_area').val(result_set['lot_area']);

                    $('#price').val(result_set['price']);
                    $('#subdivision').val(result_set['subdivision']);
                    $('#address').val(result_set['address']);
                    $('#city').val(result_set['city']);
                    $('#description').val(result_set['description']);

                    if (result_set['listing_type_id'] == 1) {
                        $('.house-and-lot-items').hide();
                        $(".house-and-lot-items input").attr("disabled", true);
                    } else if (result_set['listing_type_id'] == 2) {
                        $('.house-and-lot-items').show();
                        $(".house-and-lot-items input").attr("disabled", false);
                    } else {
                        $('.house-and-lot-items ').hide();
                    }
                }).fail(function(jqXHR, textStatus) {

                });

                $('#title-search-id').val(ui.item.value);
                $('#title').val(ui.item.label);
                return false;
            }
        });
    });
</script>
@endsection