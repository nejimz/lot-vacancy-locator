@extends('layout.master')

@section('page-title', 'Listing')

@section('content')

<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-12">
    
        <h3>Images</h3>
        @include('partials.error-messages')

        <div class="grid-x grid-padding-x">
        	
            <div class="cell large-2">

		        <form action="{{ $route }}" method="post" enctype="multipart/form-data">
                    <label>
                        Vicinity Map
                        <input type="file" name="vicinity_map" id="vicinity_map">
                    </label>

	                <label>
	                    Image
	                    <input type="file" name="image" id="image">
	                </label>
	                @csrf()
	                <button class="button success expanded margin-top-custom-40" type="submit">Upload</button>
		        	
		        </form>
            	
            </div>

            <div class="cell auto">
                <div class="grid-x grid-padding-x">
                    @if($listing->vicinity_map != '')
                    <div class="cell large-4">
                        <img class="thumbnail" src="{{ asset($listing->vicinity_map) }}">
                    </div>
                    @endif
                	@foreach($rows as $row)
                	<div class="cell large-4">
                        <a href="javascript:void(0)"><img class="thumbnail" src="{{ asset($row->location) }}"></a>
                        <div>
                            @if($row->primary == 1)
                            <a class="button tiny" href="{{ route('image.default', ['listing_id'=>$row->listing_id, 'id'=>$row->id, 'status'=>0]) }}">Default</a>
                            @else
                            <a class="button tiny secondary" href="{{ route('image.default', ['listing_id'=>$row->listing_id, 'id'=>$row->id, 'status'=>1]) }}">Default</a>
                            @endif
                            <a class="button tiny alert" href="{{ route('image.remove', ['listing_id'=>$row->listing_id, 'id'=>$row->id]) }}">Remove</a>
                        </div>
                	</div>
                	@endforeach
                </div>
            </div>

        </div>
    </div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection