@extends('layout.master')

@section('page-title', 'Listing')

@section('content')

<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-12">
        <h3>My Listing Notification</h3>
        <table width="100%">
            <thead>
                <tr>
                    <th width="10%" class="text-center">Status</th>
                    <th width="40%">Subdivision</th>
                    <th width="30%">Address</th>
                    <th width="20%" class="text-right">Price</th>
                </tr>
            </thead>
            <tbody>
            @foreach($rows as $row)
                <?php
                if(is_null($row->seen_at)) {
                    App\ListingOwner::whereId($row->id)->update(['seen_at'=>DB::raw('NOW()')]);
                }
                ?>
                <tr>
                    <td class="text-center">
                        @if (is_null($row->status))
                            AVAILABLE
                        @elseif ($row->status == 0)
                            HOLD
                        @elseif ($row->status == 1)
                            RESERVED
                        @endif
                    </td>
                    <td>{{ $row->subdivision }} {{ $row->title }}</td>
                    <td>{{ $row->address }} {{ $row->city }}</td>
                    <td class="text-right">{{ number_format($row->price, 2) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $rows->links('vendor.pagination.default') !!}
    </div>

</div>

@endsection
