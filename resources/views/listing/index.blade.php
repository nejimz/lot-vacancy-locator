@extends('layout.master')

@section('page-title', 'Listing')

@section('content')

<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-12">
        <h3>My Listing</h3>
        <form method="get" action="">
            <div class="grid-x">
                <div class="cell large-2">
                    <select name="status" id="status">
                        <option value="">All</option>
                        <option value="null">Available</option>
                        <option value="0">Hold</option>
                        <option value="1">Reserved</option>
                    </select>
                </div>
                <div class="cell large-2">
                    <select name="type" id="type">
                        <option value="">All</option>
                        @foreach($listing_types as $listing_type)
                        <option value="{{ $listing_type->id }}">{{ $listing_type->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="cell large-2">
                    <input type="text" name="search" value="{{ $search }}">
                </div>
                <div class="cell large-1">
                    <button class="button" type="submit">Search</button>
                </div>
                <div class="cell auto">&nbsp;</div>
            </div>
        </form>
        <table width="100%">
            <thead>
                <tr>
                    <th width="10%"></th>
                    <th width="10%" class="text-center">Status</th>
                    <th width="10%">Type</th>
                    <th width="25%">Subdivision</th>
                    <th width="30%">Address</th>
                    <th width="15%" class="text-right">Price</th>
                </tr>
            </thead>
            <tbody>
            @foreach($listing as $row)
                <tr>
                    <td>
                        <a href="{{ route('listing.edit', $row->id) }}" title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="{{ route('listing.image', $row->id) }}" title="Images"><i class="fa fa-image"></i></a>
                        <a href="{{ route('listing.actions', $row->id) }}" title="Action"><i class="fa fa-cog"></i></a>
                    </td>
                    <td class="text-center">{{ ucwords($row->list_status) }}</td>
                    <td>{{ $row->type->name }}</td>
                    <td>{{ $row->subdivision }} {{ $row->title }}</td>
                    <td>{{ $row->address }} {{ $row->city }}</td>
                    <td class="text-right">{{ number_format($row->price, 2) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $listing->appends(['search'=>$search])->links('vendor.pagination.default') !!}
    </div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(function(){
        $('#status').val('{{ $status }}');
        $('#type').val('{{ $type }}');
    });
</script>
@endsection