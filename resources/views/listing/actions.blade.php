@extends('layout.master')

@section('page-title', 'Listing Actions')

@section('content')

<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-12">
        <h3>Listing Actions</h3>
        @include('partials.error-messages')
        <form action="{{ $route }}" method="post">

            <div class="grid-x grid-padding-x">
                <div class="cell large-4">
                    <label>Client
                        <input type="text" id="search_client" name="search_client" value="{{ $client_name }}" placeholder="Search..." />
                        <input type="hidden" id="client_id" name="client_id" value="{{ $client_id }}" />
                    </label>
                </div>
                <div class="cell large-2">
                    <label>
                        Status
                        <select name="status" id="status">
                            <option value="">Available</option>
                            <option value="0">Hold</option>
                            <option value="1">Reserved</option>
                        </select>
                    </label>
                </div>
                <div class="cell large-6"></div>
                <div class="cell large-2">
                    <label>
                        Property Type
                        <input type="text" name="listing_type_name" id="listing_type_name" value="{{ $listing_type_name }}" readonly="">
                    </label>
                </div>
                <div class="cell large-2 lot-items">
                    <label>
                        Lot Area
                        <input type="text" name="lot_area" id="lot_area" value="{{ $lot_area }}" readonly="">
                    </label>
                </div>
                <div class="cell large-2 house-and-lot-items" style="display: none;">
                    <label>
                        Floor Area
                        <input type="text" name="floor_area" id="floor_area" value="{{ $floor_area }}" readonly="" disabled="disabled">
                    </label>
                </div>
                <div class="cell large-2 house-and-lot-items" style="display: none;">
                    <label>
                        Floor
                        <input type="number" name="floor" id="floor" value="{{ $floor }}" readonly="" disabled="disabled">
                    </label>
                </div>

                <div class="cell large-2 house-and-lot-items" style="display: none;">
                    <label>
                        Bedrooms
                        <input type="number" name="bedrooms" id="bedrooms" value="{{ $bedrooms }}" readonly="" disabled="disabled">
                    </label>
                </div>
                <div class="cell large-2 house-and-lot-items" style="display: none;">
                    <label>
                        Bathrooms
                        <input type="number" name="bathrooms" id="bathrooms" value="{{ $bathrooms }}" readonly="" disabled="disabled">
                    </label>
                </div>
            </div>

            <label>
                Subdivision
                <input type="text" name="subdivision" id="subdivision" value="{{ $subdivision }}" readonly="">
            </label>

            <label>
                Address
                <input type="text" name="address" id="address" value="{{ $address }}" readonly="">
            </label>

            <label>
                City
                <input type="text" name="city" id="city" value="{{ $city }}" readonly="">
            </label>

            <label>
                Description
                <textarea name="description" id="description" rows="5" readonly="">{{ $description }}</textarea>
            </label>
            
            <div class="grid-x">
                <div class="cell auto"></div>
                <div class="cell large-2">
                    <button class="button success expanded margin-top-custom-40" type="submit">Submit</button>
                    @csrf()
                    <a class="button expanded" href="{{ route('listing.index') }}">Back</a>
                </div>
                <div class="cell auto"></div>
            </div>
        </form>
    </div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
    $("#status").val('{{ $status }}');
    <?php
    if ($listing_type_id == 1) {
        echo '$(".house-and-lot-items").hide();';
        echo '$(".house-and-lot-items input").attr("disabled", true);';
    } elseif ($listing_type_id == 2) {
        echo '$(".house-and-lot-items").show();';
        echo '$(".house-and-lot-items input").attr("disabled", false);';
    }
    ?>
    $('#listing_type_id').val('{{ $listing_type_id }}');

    $(function(){
        $('#listing_type_id').change(function(){
            var value = $(this).val();
            if (value == 1) {
                $('.house-and-lot-items').hide();
                $(".house-and-lot-items input").attr("disabled", true);
            } else if (value == 2) {
                $('.house-and-lot-items').show();
                $(".house-and-lot-items input").attr("disabled", false);
            } else {
                $('.house-and-lot-items ').hide();
            }
        });

        var xhr_search = null;

        $("#search_client").autocomplete({
            source: function(request, response){
                xhr_search = $.ajax({
                    type : 'get',
                    url : '{{ route("inbox.search") }}',
                    cache : false,
                    dataType : "json",
                    data : "keyword=" + request.term,
                    async : false, 
                    beforeSend: function(xhr){
                        if (xhr_search != null)
                        {
                            xhr_search.abort();
                        }
                    }
                }).done(function(result_set) {
                    response($.map(result_set, function (key, value) {
                        return {
                            label: key,
                            value: value
                        };
                    }));
                }).fail(function(jqXHR, textStatus) {

                });
            },
            minLength: 2,
            select: function( event, ui ) {
                $('#client_id').val(ui.item.value);
                $('#search_client').val(ui.item.label);
                $('#status').val(1);
                return false;
            }
        });
    });
</script>
@endsection