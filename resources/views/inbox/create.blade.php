@extends('layout.master')

@section('page-title', 'Create')

@section('content')
<style type="text/css">
    .ck-content {
        height: 250px;
    }
</style>

<div class="grid-x grid-padding-x margin-top-custom-20">


    <div class="cell large-12">
        <div class="grid-x">
            <div class="cell large-12">
                <h3>Compose Message</h3>
                <form method="post" action="{{ route('inbox.store') }}">
                    <label>To
                        <input type="text" id="search_user" name="search_user" value="{{ $search_user }}" placeholder="Search..." />
                        <input type="hidden" id="user_id_to" name="user_id_to" value="{{ $user_id_to }}" />
                        <input type="hidden" id="user_name_to" name="user_name_to" value="{{ $user_name_to }}" />
                    </label>
                    <textarea name="message" id="message">{{ old('message') }}</textarea>
                    <br>
                    @csrf()
                    <button class="button" type="submit">Send Message</button>
                </form>
            </div>
        </div>

    </div>

</div>
<br>
@endsection

@section('scripts')
<script type="text/javascript">
    var xhr_search = null;
    $(function(){
        $("#search_user").autocomplete({
            source: function(request, response){
                xhr_search = $.ajax({
                    type : 'get',
                    url : '{{ route("inbox.search") }}',
                    cache : false,
                    dataType : "json",
                    data : "keyword=" + request.term,
                    async : false, 
                    beforeSend: function(xhr){
                        if (xhr_search != null)
                        {
                            xhr_search.abort();
                        }
                    }
                }).done(function(result_set) {
                    response($.map(result_set, function (key, value) {
                        return {
                            label: key,
                            value: value
                        };
                    }));
                }).fail(function(jqXHR, textStatus) {

                });
            },
            minLength: 2,
            select: function( event, ui ) {
                $('#user_id_to').val(ui.item.value);
                $('#user_name_to, #search_user').val(ui.item.label);
                return false;
            }
        });
    });

    ClassicEditor
        .create( document.querySelector( '#message' ), {
            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
        } )
        .then( editor => {
            window.editor = editor;
        } )
        .catch( err => {
            console.error( err.stack );
        } );
</script>
@endsection