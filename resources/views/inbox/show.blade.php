@extends('layout.master')

@section('page-title', 'Inbox')

@section('content')
<style type="text/css">
    .ck-content {
        height: 150px;
    }
    .h6 {
        font-size: 13px;
    }
    #message-container {
        height: 350px;
        overflow-y: scroll;
    }
</style>

<div class="grid-x grid-padding-x margin-top-custom-20">

    <div class="cell large-12">

        <div class="grid-x">
            <div class="cell large-12">
                <h4>{{ $seen->user_from->first_name . ' ' . $seen->user_from->last_name }}</h4>
                <div id="message-container" class="callout secondary small">
                    <div class="grid-x">
                        <div class="cell large-12">

                            <?php $last_row_id = ''; ?>
                            @foreach($rows as $row)
                                <div class="grid-x">
                                @if($user->id == $row->user_id_from)
                                    <div class="cell auto"></div>
                                    <div id="message-item-{{ $row->id }}" class="cell large-11">
                                        <div class="callout success small">
                                            <h6>{{ $row->user_from->first_name . ' ' . $row->user_from->last_name }}<small> {{ date('M d, Y H:i', strtotime($row->created_at)) }}</small></h6>
                                            <div class="h6">{!! $row->message !!}</div>
                                        </div>
                                    </div>
                                @else
                                    <div id="message-item-{{ $row->id }}" class="cell large-11">
                                        <div class="callout primary small">
                                            <h6>{{ $row->user_from->first_name . ' ' . $row->user_from->last_name }}<small> {{ date('M d, Y H:i', strtotime($row->created_at)) }}</small></h6>
                                            <div class="h6">{!! $row->message !!}</div>
                                        </div>
                                    </div>
                                    <div class="cell auto"></div>
                                @endif
                                </div>
                                <?php $last_row_id = $row->id; ?>
                            @endforeach

                        </div>
                    </div>
                </div>
                <form method="post" action="{{ route('inbox.store') }}">
                    <textarea name="message" id="message">{{ old('message') }}</textarea>
                    <br>
                    @csrf()
                    <button class="button" type="submit">Send Message</button>
                    <a href="{{ route('inbox.index') }}" class="button secondary" data-smooth-scroll>Back</a>
                </form>
            </div>
        </div>

    </div>

</div>
<br>
@endsection

@section('scripts')
<script type="text/javascript">

    $(function(){

        $('#message-item-{{ $last_row_id }}').attr("tabindex",-1).focus();

        $("#search_user").autocomplete({
            source: function(){
                xhr_information = $.ajax({
                    type : 'get',
                    url : '{{ route("inbox.search") }}',
                    cache : false,
                    dataType : "json",
                    data : "json=0",
                    async : false, 
                    beforeSend: function(xhr){
                        if (xhr_information != null)
                        {
                            xhr_information.abort();
                        }
                    }
                }).done(function(result_set) {

                }).fail(function(jqXHR, textStatus) {

                });
            },
            minLength: 2,
            select: function( event, ui ) {
                log( "Selected: " + ui.item.value + " aka " + ui.item.id );
            }
        });
    });

    ClassicEditor
        .create( document.querySelector( '#message' ), {
            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
        } )
        .then( editor => {
            window.editor = editor;
        } )
        .catch( err => {
            console.error( err.stack );
        } );
</script>
@endsection