@extends('layout.master')

@section('page-title', 'Inbox')

@section('content')
<style type="text/css">
    .grid-row {
        padding-top: 5px;
        padding-left: 10px;
        padding-right: 10px;
        border-bottom: 1px solid #ccc;
        background-color: #fff;
    }
    .grid-row:hover {
        background-color: #f8f8f8;
        cursor: pointer;
    }
    /*.grid-row:last-child {
        border-bottom: 1px solid #ccc;
    }*/
    .grid-row .contact-name {
        font-size: 12px;
        padding-left: 23px;
        padding-top: 0;
    }
    .msg-date {
        font-size: 12px;
    }
    .font-weight {
        font-weight: 600;
    }
</style>

<div class="grid-x grid-padding-x margin-top-custom-20">

    <div class="cell large-12">

        <div class="grid-x">
            <div class="cell large-12">
                <h3>Inbox</h3>
                <div class="grid-x">
                    <div class="cell large-12">
                        <form action="{{ route('inbox.index') }}" method="get">
                            <div class="input-group">
                                <input class="input-group-field" type="search" name="search" value="{{ $search }}" placeholder="Search..." />
                                <div class="input-group-button">
                                    <button class="button" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                                <div class="input-group-button">
                                    <button class="button success" type="button" onclick="return window.location.assign('{{ route('inbox.create') }}')"><i class="fa fa-edit"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @forelse($rows as $row)
        <a href="{{ route('inbox.show', $row->id) }}">
            <div class="grid-x grid-row">
                <div class="cell large-10">
                    <h6 class="{{ ($user->id == $row->message->user_id_to && (is_null($row->message->seen_at))? 'font-weight' : '') }}">
                        <i class="fa fa-envelope"></i> {{ $row->user_from->first_name.' '.$row->user_from->last_name }}</h6>
                    <h6 class="contact-name">{!! substr($row->message->message, 0, 20) !!}</h6>
                </div>
                <div class="cell large-2 text-right msg-date">{{ date('M d, Y H:i', strtotime($row->message_created_at)) }}</div>
            </div>
        </a>
        @empty
        <h5 class="text-center">No Messages</h5>
        @endforelse

    </div>

</div>
<br>
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection