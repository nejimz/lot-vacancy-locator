@extends('layout.master')

@section('page-title', 'Reports Listing')

@section('content')

<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell auto"></div>
    <div class="cell large-4">
        <h3>Listing Reports</h3>
        @include('partials.error-messages')
        <form action="{{ $route }}" method="get">
            <label>
                Property Type
                <select name="listing_type_id" id="listing_type_id">
                    <option value="">-- Select --</option>
                    @foreach($types as $type)
                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                    @endforeach
                </select>
            </label>
            <label>
                Status
                <select name="status" id="status">
                    <option value="">-- Select --</option>
                    <option value="available">Available</option>
                    <option value="hold">Hold</option>
                    <option value="reserved">Reserved</option>
                </select>
            </label>
            <label>
                From
                <input type="date" name="from" id="from" value="">
            </label>

            <label>
                To
                <input type="date" name="to" id="to" value="">
            </label>
            
            <div class="grid-x">
                <div class="cell auto"></div>
                <div class="cell large-4">
                    <button class="button success expanded margin-top-custom-40" type="submit">Generate</button>
                    @csrf()
                </div>
                <div class="cell auto"></div>
            </div>
        </form>
    </div>
    <div class="cell auto"></div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection