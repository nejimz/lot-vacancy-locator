@extends('layout.print')

@section('page-title', 'Reports Listing (Print)')

@section('content')
<h3 class="text-center">REAL ESTATE PROPERTY LOCATOR</h3>
From {{ date('F d, Y', strtotime($from)) }} to {{ date('F d, Y', strtotime($to)) }}
<table>
    <tr>
        <th width="10%">Status</th>
        <th width="10%">Type</th>
        <th width="40%" class="text-left">Subdivision</th>
        <th width="30%" class="text-left">Client Name</th>
    </tr>
    @foreach($listing as $row)
    <tr>
        <td class="text-center">{{ ucwords($row->list_status )}}</td>
        <td class="text-center">{{ $row->type->name }}</td>
        <td>{{ $row->subdivision }} - {{ $row->address }}</td>
        <td>{{ $row->client_name }}</td>
    </tr>
    @endforeach
</table>

@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection