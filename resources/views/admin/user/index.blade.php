@extends('layout.master')

@section('page-title', 'Users')

@section('content')

<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-12">
        <h3>Users Information</h3>
        <form method="get" action="">
            <div class="grid-x">
                <div class="cell large-2">
                    <input type="text" name="search" value="{{ $search }}">
                </div>
                <div class="cell large-1">
                    <button class="button" type="submit">Search</button>
                </div>
                <div class="cell auto">&nbsp;</div>
            </div>
        </form>
        <table width="100%">
            <thead>
                <tr>
                    <th width="8%"></th>
                    <th width="10%" class="text-center">Broker</th>
                    <th width="35%">Name</th>
                    <th width="30%">E-Mail</th>
                    <th width="17%">Registered At</th>
                </tr>
            </thead>
            <tbody>
            @foreach($rows as $row)
                <tr>
                    <td>
                        <a href="{{ route('admin.users.show', $row->id) }}" title="User Information"><i class="fa fa-edit"></i></a>
                        <a href="{{ route('admin.activity.index', ['user_id'=>$row->id]) }}" title="Log Information"><i class="fa fa-cogs"></i></a>
                    </td>
                    <td class="text-center">{{ ($row->is_broker)? 'Yes' : 'No' }}</td>
                    <td>{{ $row->first_name . ' ' . $row->last_name }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->created_at->format('M. d, Y H:i A') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $rows->appends(['search'=>''])->links('vendor.pagination.default') !!}
    </div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection