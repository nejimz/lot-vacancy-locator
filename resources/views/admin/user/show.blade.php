@extends('layout.master')

@section('page-title', 'User Information')

@section('content')


<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell auto"></div>

    <div class="cell large-4">
        <div class="callout secondary">
            <h3>Profile</h3>
            @include('partials.error-messages')
            <form action="{{ route('profile.store') }}" method="post">
                <label>
                    Email
                    <input type="email" name="email" id="email" value="{{ $row->email }}" readonly="">
                </label>
                <div class="grid-x grid-padding-x text-center">
                    <fieldset class="large-12 cell">
                        <input type="checkbox" name="is_broker" value="1" id="is_broker" disabled=""><label for="is_broker">I am a professional broker</label>
                    </fieldset>
                </div>
                <h5 class="margin-top-custom-20">Personal Information</h5>
                <label>
                    First Name
                    <input type="text" name="first_name" id="first_name" value="{{ $row->first_name }}" readonly="">
                </label>
                <label>
                    Last Name
                    <input type="text" name="last_name" id="last_name" value="{{ $row->last_name }}" readonly="">
                </label>
                <label>
                    Mobile Number
                    <input type="text" name="mobile_number" id="mobile_number" value="{{ $row->mobile_number }}" readonly="">
                </label>
                <label>
                    Phone Number
                    <input type="text" name="phone_number" id="phone_number" value="{{ $row->phone_number }}" readonly="">
                </label>
                <div class="broker" style="display: none;">
                    <h5 class="margin-top-custom-20">Agency Information</h5>
                    <label>
                        Agency Name
                        <input type="text" name="agency_name" id="agency_name" value="{{ $row->agency_name }}" readonly="">
                    </label>
                    <label>
                        Agency Address
                        <input type="text" name="agency_address" id="agency_address" value="{{ $row->agency_address }}" readonly="">
                    </label>
                    <label>
                        Agency City
                        <input type="text" name="agency_city" id="agency_city" value="{{ $row->agency_city }}" readonly="">
                    </label>
                </div> 
            </form>
        </div>
    </div>

    <div class="cell auto"></div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function(){
        <?php
        if ($row->is_broker == 1) {
            echo '$("#is_broker").attr("checked", true);';
            echo '$(".broker").show();';
        }
        ?>
        $('#is_broker').change(function(){
            var is_broker = $(this).is(':checked');
            if (is_broker) {
                $('.broker').show();
            } else {
                $('.broker').hide();
            }
        });
    });
</script>
@endsection