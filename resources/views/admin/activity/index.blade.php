@extends('layout.master')

@section('page-title', 'Users Activity Log')

@section('content')

<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-12">
        <h3>User Activity</h3>
        <form method="get" action="">
            <div class="grid-x">
                <div class="cell large-2">
                    <input type="date" name="search" value="{{ $search }}">
                </div>
                <div class="cell large-1">
                    <button class="button" type="submit">Search</button>
                </div>
                <div class="cell auto">&nbsp;</div>
            </div>
        </form>
        <table width="100%">
            <thead>
                <tr>
                    <th width="25%">User</th>
                    <th width="30%">Subdivision</th>
                    <th width="25%">Address</th>
                    <th width="20%">Seen At</th>
                </tr>
            </thead>
            <tbody>
            @foreach($rows as $row)
                <tr>
                    <td>{{ $row->user->first_name . ' ' . $row->user->last_name }}</td>
                    <td><a href="{{ route('property.show', $row->id) }}" title="Edit">{{ $row->listing->subdivision }}</a></td>
                    <td>{{ $row->listing->address }}</td>
                    <td>{{ $row->created_at->format('M. d, Y H:i A') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $rows->appends(['search'=>''])->links('vendor.pagination.default') !!}
    </div>

</div>


@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection