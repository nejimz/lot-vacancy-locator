@extends('layout.master')

@section('page-title', 'Inbox')

@section('content')
<style type="text/css">
    .grid-row {
        padding-top: 5px;
        padding-left: 10px;
        padding-right: 10px;
        border-bottom: 1px solid #ccc;
        background-color: #fff;
    }
    .grid-row:hover {
        background-color: #f8f8f8;
        cursor: pointer;
    }
    /*.grid-row:last-child {
        border-bottom: 1px solid #ccc;
    }*/
    .grid-row .contact-name {
        font-size: 12px;
        padding-left: 23px;
        padding-top: 0;
    }
    .msg-date {
        font-size: 12px;
    }
    .font-weight {
        font-weight: 600;
    }
</style>

<div class="grid-x grid-padding-x margin-top-custom-20">

    <div class="cell large-12">

        <div class="grid-x">
            <div class="cell large-12">
                <h3>Saved Property</h3>
                <div class="grid-x">
                    <div class="cell large-12">
                        @include('partials.error-messages')
                        @forelse($rows as $row)
                        <hr>
                        <div class="grid-x">
                            <div class="cell large-3 small-12">
                                <a href="">
                                    @if(is_null($row->listing->image) && $row->listing->images->where('primary', 1)->count() == 0)
                                        <img class="thumbnail" src="https://placehold.it/150x150" width="150px">
                                    @else
                                        <img class="thumbnail" src="{{ asset($row->listing->images->where('primary', 1)->first()->location) }}" width="150px">
                                    @endif
                                </a>
                                <form action="{{ route('cart.destroy', $row->id) }}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button class="button alert"><i class="fa fa-remove fa-lg"></i> Remove To List</button>
                                </form>
                            </div>
                            <div class="cell large-3 small-12">
                                <p class="lead">{{ $row->listing->subdivision }}</p>
                                <p class="subheader">{{ $row->listing->address . ', ' . $row->listing->city }}</p>
                                <p>Type: {{ $row->listing->type->name }}
                                <p>Lot Area: {{ $row->listing->lot_area }}
                                @if($row->listing->listing_type_id == 2)
                                    <br>Floor Area: {{ $row->listing->floor_area }}
                                    <br>Floor: {{ ceil($row->listing->floor) }}
                                    <br>Bedroom/s {{ ceil($row->listing->bedrooms) }}
                                    <br>Bathroom/s: {{ ceil($row->listing->bathrooms) }}
                                @endif
                                <p>{{ $row->description }}</p>
                            </div>
                            <div class="cell auto small-12">
                                <p><a href=""><i class="fa fa-envelope"></i>&nbsp;{{ $row->listing->user->first_name . ' ' . $row->listing->user->last_name }}</a></p>
                                <p>{{ $row->listing->description }}</p>
                            </div>
                        </div>
                        @empty
                        <hr>
                        <h5 class="text-center"><i class="fa fa-shopping-cart fa-lg"></i> Cart is empty!</h5>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<br>
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection