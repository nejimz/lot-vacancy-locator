@extends('layout.master')

@section('page-title', 'Property')

@section('content')
<style type="text/css">
    .thumbnail {
        width: 550px;
        height: 300px;
    }
</style>
<div class="grid-x margin-top-custom-50">
    <div class="cell large-12 small-12">
        <form action="{{ route('property.subdivision', $subdivision) }}" method="get">
            <div class="grid-x grid-padding-x">
                <div class="cell large-2 small-12">
                    <select class="input-group-field" name="type">
                        <option value="">All</option>
                        @foreach($listing_types as $listing_type)
                        <option value="{{ $listing_type->id }}" {{ ($listing_type->id == $type)? 'selected=selected':'' }}>{{ $listing_type->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="cell large-8 small-12">
                    <div class="input-group">
                        <span class="input-group-label"><i class="fa fa-map-marker fa-lg"></i></span>
                        <input class="input-group-field" type="text" name="search" placeholder="Search City or Address" value="{{ $search }}">
                    </div>
                </div>
                <div class="cell large-2 small-12">
                    <button class="button expanded" type="submit"><i class="fa fa-search"></i> <strong>Search</strong></button>
                </div>
            </div>
        </form>
        @include('partials.error-messages')
    </div>
</div>
<div class="grid-x grid-padding-x margin-top-custom-50">
    @forelse($rows as $row)
    <div class="cell large-6">
        <div class="callout">
            <a href="{{ route('property.show', $row->id) }}">
                @if(is_null($row->images->where('primary', '=', 1)->first()))
                <img class="thumbnail" src="https://placehold.it/450x300">
                @else
                <img class="thumbnail" src="{{ asset($row->images->where('primary', '=', 1)->first()->location) }}">
                @endif
            </a>
            <p class="lead"><a href="{{ route('property.show', $row->id) }}">{{ $row->subdivision }} {{ $row->title }}</a></p>
            <p class="subheader">
                <i class="fa fa-ruble"></i> <i>{{ number_format($row->price, 2) }}</i><br>
                <i class="fa fa-home"></i> <i>{{ $row->type->name }} ({{ $row->lot_area }} sqm)</i><br>
                <i class="fa fa-map-marker"></i> {{ $row->address . ', ' . $row->city }}<br>
                <i class="fa fa-calendar"></i> {{ $row->created_at->format('F d, Y') }}<br>
                <i class="fa fa-user"></i>&nbsp;
                @foreach($row->listing_owners() as $owner)
                    {{ $owner->user->full_name }},
                @endforeach
            </p>
            @if(Sentinel::check())
            <form method="post" action="{{ route('cart.store') }}">
                {{ csrf_field() }}
                <input type="hidden" name="subdivision" value="{{ $subdivision }}" />
                <input type="hidden" name="listing_id" value="{{ $row->id }}" />
                <button class="button"><i class="fa fa-star fa-lg"></i> Add to List</button>
            </form>
            @endif
        </div>
    </div>
    @empty
    <div class="cell large-12">
        <h5 class="text-center">No Records</h5>
    </div>
    @endforelse
</div>
{!! $rows->appends(['search'=>$search])->links('vendor.pagination.default') !!}

@endsection