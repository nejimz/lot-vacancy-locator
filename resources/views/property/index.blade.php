@extends('layout.master')

@section('page-title', 'Property')

@section('content')
<style type="text/css">
    .thumbnail {
        width: 550px;
        height: 300px;
    }
</style>
<h3 class="margin-top-custom-50">List of Properties</h3>
<div class="grid-x grid-padding-x margin-top-custom-20">
    @forelse($rows as $col)
    <?php
    $row = App\Listing::whereNull('status')->whereSubdivision($col->subdivision)->first();
    ?>
    <div class="cell large-6">
        <div class="callout">
            <a href="{{ route('property.subdivision', $row->subdivision) }}">
                @if(is_null($row->images->where('primary', '=', 1)->first()))
                <img class="thumbnail" src="https://placehold.it/450x300">
                @else
                <img class="thumbnail" src="{{ asset($row->images->where('primary', '=', 1)->first()->location) }}">
                @endif
            </a>
            <p class="lead"><a href="{{ route('property.subdivision', $row->subdivision) }}">{{ $row->subdivision }}</a></p>
            <p class="subheader">
                <i class="fa fa-map-marker"></i> {{ $row->address . ', ' . $row->city }}<br>
            </p>
        </div>
    </div>
    @empty
    <div class="cell large-12">
        <h5 class="text-center">No Records</h5>
    </div>
    @endforelse
</div>
{!! $rows->appends(['search'=>$search])->links('vendor.pagination.default') !!}

@endsection