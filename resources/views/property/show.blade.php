@extends('layout.master')

@section('page-title', 'Property')

@section('content')
<style type="text/css">
    .heading {
        width: 100%;
        height: 450px;
    }
    .google-map {
        width: 500px;
        width: 450px;
    }
</style>
<div class="orbit margin-top-custom-50" role="region" aria-label="Favorite Space Pictures" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">
    <div class="orbit-wrapper">
        <div class="orbit-controls">
            <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
            <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
        </div>
        <ul class="orbit-container">
            @forelse($row->images as $image)
            <li class="is-active orbit-slide">
                <figure class="orbit-figure">
                    <img class="orbit-image" src="{{ asset($image->location) }}" alt="Space" width="500px">
                </figure>
            </li>
            <?php

            if ($orbit_btn_counter == 0) {
                $orbit_btns .= '<button class="is-active" data-slide="' . $orbit_btn_counter.'"><span class="show-for-sr">' . $orbit_btn_counter.'</span></button>';
            } else {
                $orbit_btns .= '<button data-slide="' . $orbit_btn_counter.'"><span class="show-for-sr">' . $orbit_btn_counter.'</span></button>';
            }

            $orbit_btn_counter++;
            ?>
            @empty
            <li class="is-active orbit-slide">
                <figure class="orbit-figure">
                    <img class="thumbnail heading" src="https://placehold.it/750x300">
                </figure>
            </li>
            @endforelse
        </ul>
    </div>
</div>

<div class="grid-x margin-top-custom-20">
    <div class="cell large-7">
        <h4>{{ $row->subdivision }}</h4>
        <p class="subheader">
            <i class="fa fa-home"></i> <i>{{ $row->type->name }} ({{ $row->lot_area }} sqm)</i><br>
            <i class="fa fa-map-marker"></i> {{ $row->address . ', ' . $row->city }}<br>
            <i class="fa fa-calendar"></i> {{ $row->created_at->format('F d, Y') }}<br>
            <i class="fa fa-user"></i>
            @foreach($row->listing_owners() as $owner)
                @if(Sentinel::check() && Sentinel::getUser()->id != $row->user_id)
                    <a href="{{ route('inbox.create', ['user_id'=>$row->user_id]) }}">{{ $owner->user->full_name }}</a>, 
                @else
                    {{ $owner->user->full_name }}, 
                @endif
            @endforeach
            <br>
            <i class="fa fa-eye"></i> {{ $row->views() }}
        </p>
        <h5>Description</h5>
        <p>{{ $row->description }}</p>
        @if(Sentinel::check())
        <div class="grid-x ">
            <div class="cell large-2">
                <form method="post" action="{{ route('cart.store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="listing_id" value="{{ $row->id }}" />
                    <button class="button success"><i class="fa fa-star fa-lg"></i> Add to List</button>
                </form>
                
            </div>
        </div>
        @endif
    </div>
    <div class="cell large-5">

        <img class="thumbnail google-map" src="{{ ($row->vicinity_map != '')? asset($row->vicinity_map) : 'https://placehold.it/500x450' }}" width="100%">
    </div>
</div>

@endsection