@extends('layout.master')

@section('page-title', 'Register')

@section('content')


<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell auto"></div>

    <div class="cell large-4">
        <div class="callout secondary">
            <h3>Registration</h3>
            @include('partials.error-messages')
            <form action="{{ route('register.store') }}" method="post">
                <label>
                    Email
                    <input type="email" name="email" id="email" value="{{ $email }}">
                </label>
                <label>
                    Password
                    <input type="password" name="password" id="password">
                </label>
                <label>
                    Password Confirm
                    <input type="password" name="password_confirm" id="password_confirm">
                </label>
                <div class="grid-x grid-padding-x text-center">
                    <fieldset class="large-12 cell">
                        <input type="checkbox" name="is_broker" value="1" id="is_broker"><label for="is_broker">I am a professional broker</label>
                    </fieldset>
                </div>
                <h5 class="margin-top-custom-20">Personal Information</h5>
                <label>
                    First Name
                    <input type="text" name="first_name" id="first_name" value="{{ $first_name }}">
                </label>
                <label>
                    Last Name
                    <input type="text" name="last_name" id="last_name" value="{{ $last_name }}">
                </label>
                <label>
                    Mobile Number
                    <input type="text" name="mobile_number" id="mobile_number" value="{{ $mobile_number }}">
                </label>
                <label>
                    Phone Number
                    <input type="text" name="phone_number" id="phone_number" value="{{ $phone_number }}">
                </label>
                <div class="broker" style="display: none;">
                    <h5 class="margin-top-custom-20">Agency Information</h5>
                    <label>
                        Agency Name
                        <input type="text" name="agency_name" id="agency_name" value="{{ $agency_name }}" disabled>
                    </label>
                    <label>
                        Agency Address
                        <input type="text" name="agency_address" id="agency_address" value="{{ $agency_address }}" disabled>
                    </label>
                    <label>
                        Agency City
                        <input type="text" name="agency_city" id="agency_city" value="{{ $agency_city }}" disabled>
                    </label>
                </div> 
                @csrf()
                <button class="button success expanded margin-top-custom-40" type="submit">Submit</button>
                <a class="button expanded" href="{{ route('login.index') }}">Log In</a>
            </form>
        </div>
    </div>

    <div class="cell auto"></div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function(){
        <?php
        if ($is_broker == 1) {
            echo '$("#is_broker").attr("checked", true);';
            echo '$(".broker").show();';
            echo '$(".broker label input").attr("disabled", false);';
        }
        ?>
        $('#is_broker').change(function(){
            var is_broker = $(this).is(':checked');
            if (is_broker) {
                $('.broker').show();
                $('.broker label input').attr('disabled', false);
            } else {
                $('.broker').hide();
                $('.broker label input').attr('disabled', true);
            }
        });
    });
</script>
@endsection