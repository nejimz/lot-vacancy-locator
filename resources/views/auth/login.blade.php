@extends('layout.master')

@section('page-title', 'Home')

@section('content')

<div class="grid-x grid-padding-x  margin-top-custom-40">

    <div class="cell auto"></div>

    <div class="cell large-4">
        <div class="callout secondary">
            <h3>Login</h3>
            @include('partials.error-messages')
            <form action="{{ route('login.authenticate') }}" method="post">
                <label>
                    Username
                    <input type="text" name="email" id="email">
                </label>
                <label>
                    Password
                    <input type="password" name="password" id="password">
                </label>
                @csrf()
                <button class="button expanded" type="submit">Log In</button>
                <p class="text-center">
                    <a href="">Forgot password?</a>
                </p>
                <p class="text-center">or</p>
                <a class="button success expanded" href="{{ route('register.index') }}">Register</a>
            </form>
        </div>
    </div>

    <div class="cell auto"></div>

</div>
@endsection