@extends('layout.master')

@section('page-title', 'Broker`s')

@section('content')
<style type="text/css">
    .fa-star-color {
        color: #f7c42d!important;
    }
    .thumbnail {
        width: 250px!important;
    }
</style>

<div class="grid-x grid-padding-x margin-top-custom-30">

    <div class="cell large-12">
		<h3>Our Broker`s</h3>

		<div class="grid-x">
            @foreach($rows as $row)
            <div class="cell large-4">
                <img class="thumbnail" src="{{ (is_null($row->image)) ? 'https://placehold.it/250x400' : $row->image }}">
                <p class="lead">
                	<a href="{{ route('brokers.show', $row->id) }}">{{ $row->first_name . ' ' . $row->last_name }}</a>
                </p>
                <p class="subheader">
                	Agency Name : {{ $row->agency_name }}<br>
                	Member Since : {{ $row->created_at->format('F d, Y') }}<br>
                	Ratings : {!! $row->rating !!}
                </p>
                <br>
            </div>
			@endforeach
		</div>

        {!! $rows->appends(['search'=>''])->links('vendor.pagination.default') !!}
    </div>

</div>

@endsection