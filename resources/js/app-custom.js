
function error_message(responseJSON)
{
	var error_string = '';
	$.each(responseJSON, function(key, value){
		error_string += '' + value[0] + '<br />';
	});

	return error_string;
}

function increment_decrement(id, max, type)
{
	var field = $('#' + id).val();

	if(isNaN(field)) {
		field = 1;
		alert('Input is not a number!');
	} else {
		field = parseInt(field);

		if (type == 'inc') {
			if (field < max || max == 0) {
				field++;
			}
		} else if (type == 'dec') {
			field--;
			if (field < 1) {
				field = 1;
			}
		} else {

			if (max == 0) {
				field = field;
			} else if (field > max) {
				field = max;
			} else if (field < 1) {
				field = 1;
			}
		}
		$('.increment_decrement_validation_msg').html('');
	}
	$('#' + id).focus().val(field).change();//.trigger( 'change' );
	return false;
}