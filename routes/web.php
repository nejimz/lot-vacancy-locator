<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController@index')->name('index');

Route::resource('property', 'PropertyController');
Route::get('property/{subdivision}/subdivision', 'PropertyController@subdivision')->name('property.subdivision');

Route::resource('brokers', 'BrokersController');
Route::post('brokers/{broker_id}/rate', 'BrokersController@rate')->name('brokers.rate');

Route::prefix('register')->name('register.')->group(function(){
	Route::get('/', 'RegistrationController@index')->name('index');
	Route::post('store', 'RegistrationController@store')->name('store');
});

Route::prefix('login')->name('login.')->group(function(){
	Route::get('/', 'AuthenticationController@index')->name('index');
	Route::post('authenticate', 'AuthenticationController@authentication')->name('authenticate');
	Route::get('logout', 'AuthenticationController@logout')->middleware('authenticated.user')->name('logout');
});

Route::middleware('authenticated.user')->group(function(){
	Route::middleware('admin.user')->prefix('admin')->name('admin.')->group(function(){
		Route::resource('users', 'UsersController');
		Route::get('activity', 'UserActivityController@index')->name('activity.index');
	});

	Route::prefix('profile')->name('profile.')->group(function(){
		Route::get('/', 'ProfileController@index')->name('index');
		Route::post('store', 'ProfileController@store')->name('store');
	});

	Route::resource('cart', 'CartController');

	Route::middleware('is_broker')->group(function(){
		Route::resource('listing', 'ListingController');
		Route::get('listing/actions/{id}', 'ListingController@actions')->name('listing.actions');
		Route::post('listing/actions-submit/{id}', 'ListingController@actions_submit')->name('listing.actions_submit');

		Route::get('listing/{id}/image', 'ListingController@image')->name('listing.image');
		Route::post('listing/{id}/image', 'ListingController@image_upload')->name('listing.image_upload');
		Route::get('listing/{listing_id}/image/{id}/remove', 'ListingController@remove')->name('image.remove');
		Route::get('listing/{listing_id}/image/{id}/default/{status}', 'ListingController@default')->name('image.default');
	});
	Route::get('unit-address-search', 'ListingController@search')->name('listing.unit-address-search');
	Route::get('unit-address-select', 'ListingController@select')->name('listing.unit-address-select');

	Route::get('listing-viewed', 'ViewedController@index')->name('listing.viewed');


	Route::resource('inbox', 'InboxController');
	Route::get('user/search', 'InboxController@search')->name('inbox.search');
	Route::get('notification', 'ViewedController@notification')->name('viewed.notification');

	Route::prefix('reports')->name('reports.')->group(function(){
		Route::get('listing', 'ReportsListingController@index')->name('listing');
		Route::get('listing-print', 'ReportsListingController@print')->name('listing-print');
	});

});