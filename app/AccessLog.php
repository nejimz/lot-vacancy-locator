<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessLog extends Model
{
    protected $connection = 'mysql';
    protected $table = 'access_logs';
    protected $guarded = [];

    public function listing()
    {
    	return $this->hasOne('App\Listing', 'id', 'listing_id');
    }

    public function user()
    {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
