<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingAction extends Model
{
    protected $connection = 'mysql';
    protected $table = 'listing_actions';
    protected $guarded = [];

    public function listing()
    {
    	return $this->belongsTo('App\Listing', 'id', 'listing_type_id');
    }

    public function client()
    {
    	return $this->hasOne('App\User', 'id', 'client_id');
    }
}
