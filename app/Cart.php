<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $connection = 'mysql';
    protected $table = 'carts';
    protected $guarded = [];

    public function listing()
    {
    	return $this->hasOne('App\Listing', 'id', 'listing_id');
    }

    public function user()
    {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
