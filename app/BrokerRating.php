<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrokerRating extends Model
{
    protected $connection = 'mysql';
    protected $table = 'broker_ratings';
    protected $guarded = [];

    /*public function user()
    {
    	return $this->hasMany('App\User', 'id', 'user_id');
    }*/
}
