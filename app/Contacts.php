<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $connection = 'mysql';
    protected $table = 'contacts';
    protected $guarded = [];

    public function message()
    {
    	return $this->belongsTo('App\ContactsMessages', 'id', 'contact_id');
    }

    public function user_from()
    {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function user_to()
    {
    	return $this->hasOne('App\User', 'id', 'contact_user_id');
    }

}
