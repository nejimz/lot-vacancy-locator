<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactsMessages extends Model
{
    protected $connection = 'mysql';
    protected $table = 'contacts_messages';
    protected $guarded = [];

    public function contact()
    {
    	return $this->hasOne('App\Contacts', 'id', 'contact_id');
    }

    public function user_from()
    {
    	return $this->hasOne('App\User', 'id', 'user_id_from');
    }

    public function user_to()
    {
    	return $this->hasOne('App\User', 'id', 'user_id_to');
    }
}
