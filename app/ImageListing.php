<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageListing extends Model
{
    protected $connection = 'mysql';
    protected $table = 'image';
    protected $guarded = [];

    public function listing()
    {
        return $this->hasOne('App\Listing', 'id', 'listing_id');
    }

    public function owner()
    {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
