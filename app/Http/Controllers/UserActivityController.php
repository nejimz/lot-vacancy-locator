<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AccessLog;
use Sentinel;
use DB;

class UserActivityController extends Controller
{
    public function index(Request $request)
    {
        $search = date('Y-m-d');
        $user = Sentinel::getUser();
        #$query = Accesslog::with(['listing', 'user']);
        $query = AccessLog::with('listing', 'user');

        if ($request->has('user_id')) {
        	$query = $query->whereUserId($request->user_id);
        }

        if ($request->has('search')) {
            $search = date('Y-m-d', strtotime($request->search));
            $query = $query->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') = '$search'");
        }

        $rows = $query->orderBy('created_at', 'DESC')->paginate(50);
        #$rows = $query->orderBy('created_at', 'DESC')->toSql();dd($rows);
        
        $data = compact('rows', 'search');
        return view('admin.activity.index', $data);
    }
}
