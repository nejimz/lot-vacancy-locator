<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImageListing;
use App\Listing;
use App\ListingOwner;
use App\ListingType;
use App\ListingAction;
use Sentinel;
use Illuminate\Support\Str;
use Storage;
use \Carbon\Carbon;

class ListingController extends Controller
{
    public function index(Request $request)
    {
        $status = '';
        $type = '';
        $search = '';
        #$listing_query = Listing::whereUserId(Sentinel::getUser()->id);
        $user_id = Sentinel::getUser()->id;
        $listing_query = Listing::whereIn('id', function($query) use ($user_id){
            return $query->select('listing_id')->from('listing_owners')->where('user_id', $user_id);
        });

        if ($request->has('search')) {
            $status = $request->status;
            $type = $request->type;
            $search = $request->search;

            if ($status != '') {
                if ($status == 'null') {
                    $listing_query = $listing_query->whereNull('status');
                } else {
                    $listing_query = $listing_query->whereStatus($status);
                }
            }

            if ($type != '') {
                $listing_query = $listing_query->whereListingTypeId($type);
            }

            if ($search != '') {
                $listing_query = $listing_query->where(function($query) use ($search){
                    $query->where('subdivision', 'LIKE', "%$search%")->orWhere('address', 'LIKE', "%$search%")->orWhere('city', 'LIKE', "%$search%");
                });
            }

        }
        $listing = $listing_query->paginate(50);
        #$listing = $listing_query->toSql();dd($listing);
        $listing_types = ListingType::get();

        $data = compact('listing', 'listing_types', 'search', 'status', 'type');
        return view('listing.index', $data);
    }

    public function create(Request $request)
    {
        $route = route('listing.store');
        $title = old('title');
        $price = old('price');
        $listing_type_id = old('listing_type_id');
        $lot_area = old('lot_area');
        $floor_area = old('floor_area');
        $floor = old('floor');
        $bedrooms = old('bedrooms');
        $bathrooms = old('bathrooms');
        $subdivision = old('subdivision');
        $address = old('address');
        $city = old('city');
        $description = old('description');

        $types = ListingType::get();

        $data = compact('route', 'title', 'price', 'listing_type_id', 'lot_area', 'floor_area', 'floor', 'bedrooms', 'bathrooms', 'subdivision', 'address', 'city', 'description', 'types');

        return view('listing.form', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'listing_type_id' => 'required',
            'title' => 'required',
            'price' => 'required|numeric',
            'lot_area' => 'required',
            'floor_area' => 'required_if:listing_type_id,2|numeric',
            'floor' => 'required_if:listing_type_id,2',
            'bedrooms' => 'required_if:listing_type_id,2',
            'bathrooms' => 'required_if:listing_type_id,2',
            'subdivision' => 'required',
            'address' => 'required',
            'city' => 'required',
            'description' => 'required'
        ],[
            'floor.required_if' => 'The floor field is required.',
            'bedrooms.required_if' => 'The bedrooms field is required.',
            'bathrooms.required_if' => 'The bathrooms field is required.'
        ]);

        $data = $request->except('_token', 'title_search_id');
        $data['user_id'] = Sentinel::getUser()->id;
        $id = $request->title_search_id;
        if ($request->title_search_id == 0) {
            $listing = Listing::create($data);
            $id = $listing->id;
        #} else {
            #ListingOwner::create(['listing_id' => $request->title_search_id, 'user_id' => Sentinel::getUser()->id]);
            #return redirect()->back()->with('success_msg', 'listing successfuly added.');
        }
        ListingOwner::create(['listing_id' => $id, 'user_id' => Sentinel::getUser()->id]);
        return redirect()->back()->with('success_msg', 'You successfuly created a new listing.');
    }

    public function edit($id, Request $request)
    {
        $row = Listing::find($id);

        if (!$row) {
            abort(404);
        }

        $route = route('listing.update', $id);

        $title = $row->title;
        $price = $row->price;
        $listing_type_id = $row->listing_type_id;
        $lot_area = $row->lot_area;
        $floor_area = $row->floor_area;
        $floor = $row->floor;
        $bedrooms = $row->bedrooms;
        $bathrooms = $row->bathrooms;
        $subdivision = $row->subdivision;
        $address = $row->address;
        $city = $row->city;
        $description = $row->description;

        $types = ListingType::get();

        $data = compact('route', 'id', 'title', 'price', 'listing_type_id', 'lot_area', 'floor_area', 'floor', 'bedrooms', 'bathrooms', 'subdivision', 'address', 'city', 'description', 'types');

        return view('listing.form', $data);
    }

    public function update($id, Request $request)
    {
        $row = Listing::find($id);

        if (!$row) {
            abort(404);
        }

        $request->validate([
            'listing_type_id' => 'required',
            'title' => 'required',
            'price' => 'required|numeric',
            'lot_area' => 'required',
            'floor_area' => 'required_if:listing_type_id,2|numeric',
            'floor' => 'required_if:listing_type_id,2',
            'bedrooms' => 'required_if:listing_type_id,2',
            'bathrooms' => 'required_if:listing_type_id,2',
            'subdivision' => 'required',
            'address' => 'required',
            'city' => 'required',
            'description' => 'required'
        ],[
            'floor.required_if' => 'The floor field is required.',
            'bedrooms.required_if' => 'The bedrooms field is required.',
            'bathrooms.required_if' => 'The bathrooms field is required.'
        ]);

        $data = $request->except('_token', '_method');

        Listing::whereId($id)->update($data);

        return redirect()->back()->with('success_msg', 'You successfuly created a new listing.');
    }

    public function show($id)
    {
        return view('listing.show');
    }

    public function image($id)
    {
        $listing = Listing::find($id);
        $rows = ImageListing::whereListingId($id)->paginate(50);

        if (!$rows) {
            abort(404);
        }

        $route = route('listing.image_upload', $id);

        $data = compact('listing', 'rows', 'route');
        return view('listing.image', $data);
    }

    public function image_upload($id, Request $request)
    {
        $request->validate([
            'vicinity_map' => 'image',
            'image' => 'image'
        ]);
        $user           = Sentinel::getUser();

        if($request->has('vicinity_map')) {
            $vicinity_map   = $request->vicinity_map;
            $vicinity_map_extension = $request->vicinity_map->getClientOriginalExtension();
            $vicinity_map_name      = date('Ymdhis') . '_' . Str::random(10) . '.' . $vicinity_map_extension;
            $vicinity_map_location  = 'images/vicinity-map/'.$id;

            if (Storage::exists($vicinity_map_location)) {
                Storage::deleteDirectory($vicinity_map_location);
            }
            Storage::makeDirectory($vicinity_map_location);

            $request->vicinity_map->move(public_path($vicinity_map_location), $vicinity_map_name);

            Listing::whereId($id)->update([
                'vicinity_map'=>$vicinity_map_location.'/'.$vicinity_map_name
            ]);
        }

        if($request->has('image')) {
            $file           = $request->image;
            $extension      = $request->image->getClientOriginalExtension();
            $file_name      = date('Ymdhis') . '_' . Str::random(10) . '.' . $extension;
            $file_location  = 'images/listing/'.$id;

            if (Storage::exists($file_location)) {
                Storage::deleteDirectory($file_location);
            }
            Storage::makeDirectory($file_location);

            $request->image->move(public_path($file_location), $file_name);

            ImageListing::create([
                'listing_id'=>$id,
                'user_id'=>$user->id,
                'location'=>$file_location.'/'.$file_name
            ]);
        }

        return redirect()->route('listing.image_upload', $id)->with('success_msg', 'Image successfully uploaded!');
    }

    public function remove($listing_id, $id)
    {
        $row = ImageListing::whereId($id)->first();

        if (!$row) {
            abort(404);
        }
        ImageListing::whereId($id)->delete();
        return redirect()->route('listing.image', $listing_id)->with('success_msg', 'Image successfully remove!');
    }

    public function default($listing_id, $id, $status)
    {
        $row = ImageListing::whereId($id)->first();

        if (!$row) {
            abort(404);
        }

        ImageListing::whereListingId($listing_id)->update(['primary'=>0]);
        if ($status <= 0) {
            $status = 0;
        } else {
            $status = 1;
        }
        ImageListing::whereId($id)->update(['primary'=>1]);
        return redirect()->route('listing.image', $listing_id)->with('success_msg', 'Primary Image successfully set!');
    }

    public function actions($id)
    {
        $route = route('listing.actions_submit', $id);
        $row = Listing::find($id);

        $status = $row->status;
        $listing_type_name = $row->type->name;
        $listing_type_id = $row->listing_type_id;
        $lot_area = $row->lot_area;
        $floor_area = $row->floor_area;
        $floor = $row->floor;
        $bedrooms = $row->bedrooms;
        $bathrooms = $row->bathrooms;
        $subdivision = $row->subdivision;
        $address = $row->address;
        $city = $row->city;
        $description = $row->description;

        $client_id = '';
        $client_name = '';
        if ($status == 1) {
            $action = ListingAction::with('client')->whereListingId($id)->orderBy('created_at', 'DESC')->first();
            $client_id = $action->client_id;
            $client_name = $action->client->full_name;
        }

        $types = ListingType::get();

        $data = compact('route', 'status', 'listing_type_name', 'listing_type_id', 'lot_area', 'floor_area', 'floor', 'bedrooms', 'bathrooms', 'subdivision', 'address', 'city', 'description', 'client_id', 'client_name', 'types');

        return view('listing.actions', $data);
    }

    public function actions_submit($id, Request $request)
    {
        $row = Listing::find($id);

        if (!$row) {
            abort(404);
        }

        $request->validate([
            'client_id' => 'required_if:status,1'
        ],[
            'client_id.required_if' => 'Client field is required.'
        ]);

        $data = $request->only('status', 'client_id');
        $data['listing_id'] = $id;
        $data['user_id'] = Sentinel::getUser()->id;
        $seen_at = null;
        
        if(is_null($request->status)) {
            $status = 'Available';
            unset($data['client_id']);
        } elseif ($request->status == 0) {
            $status = 'Hold';
        } elseif ($request->status == 1) {
            $status = 'Reserved';
            $seen_at = Carbon::now();
        }

        Listing::whereId($id)->update(['status' => $request->status]);
        ListingOwner::whereUserId(Sentinel::getUser()->id)->whereListingId($id)->update(['seen_at' => $seen_at]);
        ListingAction::create($data);

        return redirect()->route('listing.actions', $id)->with('success_msg', 'Property has changed to ' . $status . '.');
    }

    public function search(Request $request) {
        $rows = [];
        if ($request->has('keyword')) {
            $keyword = request('keyword');
            $rows = Listing::where('title', 'LIKE', "%$keyword%")->pluck('title', 'id');
        }
        return response()->json($rows);
    }

    public function select(Request $request) {
        $row = [];
        if ($request->has('id')) {
            $row = Listing::find($request->id);
        }
        return response()->json($row);
    }
}
