<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sentinel;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Users\Eloquent\User;
use Cartalyst\Sentinel\Users\UserNotFoundException;

class AuthenticationController extends Controller
{
    public function index()
    {
    	return view('auth.login');
    }

    public function Authentication(Request $request)
    {
        try {

            $remember = false;
            $credentials = $request->only('email', 'password');
            Sentinel::enableCheckpoints();
            $authenticate = Sentinel::authenticate($credentials, $remember);

            if (!$authenticate) {
                return redirect()->back()->withErrors(['email' => 'Invalid username or password.']);
            }
            return redirect()->route('index');
        } catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {
            return redirect()->back()->withErrors(['email' => "User is suspended for " . ceil($e->getDelay() / 60) . " minutes."]);
        } catch (NotActivatedException $e) {
            return redirect()->back()->withErrors(['email' => 'Your account has not been activated yet.']);
        }

    	return redirect()->back();
    }
    
    public function logout()
    {
    	Sentinel::logout();
        return redirect()->route('index');
    }
}
