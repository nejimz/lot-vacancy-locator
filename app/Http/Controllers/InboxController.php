<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactsMessages;
use App\Contacts;
use App\User;
use \Carbon\Carbon;
use Sentinel;
use DB;

class InboxController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        $user = Sentinel::getUser();
        $query = Contacts::select('id', 'user_id', 'contact_user_id', DB::raw('(SELECT created_at FROM contacts_messages WHERE contact_id=contacts.id ORDER BY created_at DESC LIMIT 1) message_created_at'));

        if ($request->has('search') && $request->search != '') {
            $search = $request->search;
            $query = $query->whereHas('message', function($query) use ($search){
                $query->where('message', 'LIKE', "%$search%");
            })->orWhereHas('user_to', function($query) use ($search){
                $query->where('first_name', 'LIKE', "%$search%")->orWhere('last_name', 'LIKE', "%$search%");
            });
        }
        $rows = $query->where(function($query) use ($user){
            $query->where('user_id', $user->id)->orWhere('Contact_user_id', $user->id);
        })->orderBy('message_created_at', 'DESC')->get();
        #dd($user);
        $data = compact('search', 'user', 'rows');
    	return view('inbox.index', $data);
    }

    public function create(Request $request)
    {
        $search_user = old('to');
        $user_id_to = old('user_id_to');
        $user_name_to = old('user_name_to');
        if ($request->has('user_id')) {
            $user = User::find($request->user_id);
            $user_id_to = $user->id;
            $user_name_to = $user->first_name . '' . $user->last_name;
            $search_user = $user_name_to;
        }
        $data = compact('search_user', 'user_id_to', 'user_name_to');
    	return view('inbox.create', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id_to' => 'required',
            'user_name_to' => 'required',
            'message' => 'required'
        ]);
        $user = Sentinel::getUser();

        $contact = Contacts::whereUserId($user->id)->whereContactUserId($request->user_id_to)->first();

        if (is_null($contact)) {
            $contact = Contacts::create([ 'contact_user_id' => $request->user_id_to, 'user_id' => $user->id ]);
        }

        ContactsMessages::create([
            'contact_id' => $contact->id, 
            'user_id_to' => $contact->contact_user_id, 
            'user_id_from' => $contact->user_id,
            'message' => $request->message 
        ]);

        return redirect()->route('inbox.index')->with('success', 'Message successfully sent!');
    }

    public function show($id)
    {
        $user = Sentinel::getUser();
        #$seen = ContactsMessages::whereContactId($id)->whereUserIdTo($user->id)->orderBy('seen_at', 'ASC')->first();
        $seen = ContactsMessages::whereContactId($id)->where(function($query) use ($user){
            $query->where('user_id_to', $user->id)->orWhere('user_id_from', $user->id);
        })->orderBy('seen_at', 'ASC')->first();

        if (!is_null($seen) && is_null($seen->seen_at)) {
            $seen->seen_at = Carbon::now();
            $seen->save();
        }

        $rows = ContactsMessages::whereContactId($id)->orderBy('created_at', 'ASC')->paginate(20);
        $data = compact('user', 'seen', 'rows');
        return view('inbox.show', $data);
    }

    public function edit($id, Request $request)
    {

    }

    public function update($id, Request $request)
    {

    }

    public function search(Request $request)
    {
        $keyword = request('keyword');
        $rows = [];
        if ($request->has('keyword')) {
            $rows = User::select('id', DB::raw('CONCAT(first_name, \' \', last_name) AS user_name'))->where('id', '<>', Sentinel::getUser()->id)
                    ->where(function($query) use ($keyword){
                        $query->where('first_name', 'LIKE', "%$keyword%")->orWhere('last_name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%");
                    })->pluck('user_name', 'id');
        }
        return response()->json($rows);
    }
}
