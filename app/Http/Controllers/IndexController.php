<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;

class IndexController extends Controller
{
    public function index()
    {
    	$properties = Listing::whereNull('status')->orderBy('created_at', 'DESC')->take(15)->get();
    	$data = compact('properties');
    	return view('index', $data);
    }
}
