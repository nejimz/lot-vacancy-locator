<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
	public function index(Request $request)
	{
		$search = '';
		$query = new User;
		if ($request->has('search') && $request->search != '') {
			$search = $request->search;
			$query = $query->where('first_name', 'LIKE', "%$search%")->orWhere('last_name', 'LIKE', "%$search%");
		}
		$rows = $query->paginate(50);
		$data = compact('rows', 'search');
		return view('admin.user.index', $data);
	}

	public function show($id)
	{
		$row = User::find($id);
		if(is_null($row)) {
			abort(404);
		}
		$data = compact('row');
		return view('admin.user.show', $data);
	}
}
