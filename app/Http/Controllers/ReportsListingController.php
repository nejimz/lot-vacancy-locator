<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;
use App\ListingType;
use App\ListingAction;
use Sentinel;

class ReportsListingController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        $type = '';
        $route = route('reports.listing-print');
        $types = ListingType::get();
        $data = compact('types', 'route');

        return view('reports.listing', $data);
    }

    public function print(Request $request)
    {
    	$from = date('Y-m-d 00:00', strtotime($request->from));
    	$to = date('Y-m-d 23:59', strtotime($request->to));

    	$lquery = Listing::whereUserId(Sentinel::getUser()->id);
    	#$lquery = Listing::whereUserId(5);

    	if ($request->listing_type_id != '') {
    		$lquery = $lquery->where('listing_type_id', $request->listing_type_id);
    	}

    	if ($request->status == 'hold') {
    		$lquery = $lquery->whereStatus(0);
    	} elseif ($request->status == 'reserved') {
    		$lquery = $lquery->whereStatus(1);
    	} elseif ($request->status == 'available') {
    		$lquery = $lquery->whereNull('status');
    	}

    	$listing = $lquery->where(function($query) use ($from, $to){
    		return $query->where('updated_at', '>=', $from)->where('updated_at', '<=', $to);
    	})
    	->orderBy('created_at', 'ASC')->get();
		#->orderBy('created_at', 'ASC')->toSql();

    	#dd($listing);

    	$data = compact('listing', 'from', 'to');
        return view('reports.listing-print', $data);
    }
}
