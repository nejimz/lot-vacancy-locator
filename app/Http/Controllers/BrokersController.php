<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Listing;
use App\BrokerRating;
use Sentinel;

class BrokersController extends Controller
{
    public function index(Request $request)
    {
    	$rows = User::whereIsBroker(1)->whereNull('is_admin')->paginate(50);
    	$data = compact('rows');
        return view('brokers', $data);
    }

    public function show($id)
    {
    	$broker = User::whereId($id)->whereIsBroker(1)->whereNull('is_admin')->first();

    	if (is_null($broker)) {
    		abort(404);
    	}
        
    	$rows = Listing::whereUserId($id)->paginate(50);

    	$data = compact('broker', 'rows');
        return view('account.broker', $data);
    }

    public function rate(Request $request, $broker_id)
    {
        $request->validate(['rate'=>'required']);
        BrokerRating::create([
            'user_id' => Sentinel::getUser()->id,
            'broker_id' => $broker_id,
            'rating' => $request->rate
        ]);
        return redirect()->route('brokers.show', $broker_id)->with('success', 'Thank you for rating!');
    }
}
