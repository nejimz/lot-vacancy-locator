<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImageListing;
use App\AccessLog;
use App\Listing;
use App\ListingType;
use Sentinel;
use DB;

class PropertyController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        $type = '';
        $listing_query = Listing::select('subdivision')->whereStatus(null);

        if ($request->has('search')) {
            $type = $request->type;
            $search = $request->search;
            if ($type != '') {
                $listing_query = $listing_query->whereListingTypeId($type);
            }
            $listing_query = $listing_query->where(function($query) use ($search){
                $query->where('subdivision', 'LIKE', "%$search%")->orWhere('address', 'LIKE', "%$search%")->orWhere('city', 'LIKE', "%$search%");
            });
        }
        $rows = $listing_query->groupBy('subdivision')->orderBy('subdivision', 'ASC')->paginate(50);
        #$rows = $listing_query->groupBy('subdivision')->orderBy('created_at', 'DESC')->toSql();
        $listing_types = ListingType::get();
        #dd($rows);
        $data = compact('rows', 'listing_types', 'search', 'type');

        return view('property.index', $data);
    }

    public function show($id)
    {
        $row = Listing::find($id);
        #dd($row);
        if (is_null($row) || !is_null($row->status)) {
            abort(404);
        }

        if (Sentinel::check()) {
            $user = Sentinel::getUser();
            $date_check = AccessLog::whereListingId($id)->whereId($user->id)->where(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"), date('Y-m-d'))->count();
            
            if ($date_check == 0) {
                AccessLog::create(['listing_id'=>$id, 'user_id'=>$user->id]);
            }
        }

        $orbit_btns = '';
        $orbit_btn_counter = 0;

        $data = compact('row', 'orbit_btns', 'orbit_btn_counter');

        return view('property.show', $data);
    }

    public function subdivision(Request $request, $subdivision)
    {
        $search = '';
        $type = '';
        $listing_query = Listing::whereSubdivision($subdivision)->whereStatus(null);
        if ($request->has('search')) {
            $type = $request->type;
            $search = $request->search;
            if ($type != '') {
                $listing_query = $listing_query->whereListingTypeId($type);
            }
            $listing_query = $listing_query->where(function($query) use ($search){
                $query->where('subdivision', 'LIKE', "%$search%")->orWhere('address', 'LIKE', "%$search%")->orWhere('city', 'LIKE', "%$search%");
            });
        }

        $rows = $listing_query->orderBy('created_at', 'DESC')->paginate(50);
        #$rows = $listing_query->orderBy('created_at', 'DESC')->toSql();
        #dd($rows);
        $listing_types = ListingType::get();

        $data = compact('subdivision', 'rows', 'listing_types', 'search', 'type');

        return view('property.subdivision', $data);
    }
}
