<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use Sentinel;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $user = Sentinel::getUser();
    	$rows = Cart::whereUserId($user->id)->get();

    	$data = compact('rows');
    	return view('cart.index', $data);
    }

    public function store(Request $request)
    {
    	$user_id = Sentinel::getUser()->id;
    	$check = Cart::whereListingId($request->listing_id)->whereUserId($user_id)->count();
    	if ($check > 0) {
    		return redirect()->route('cart.index')->with('custom_error', 'Property already exists!');
    	}

		Cart::create([
			'listing_id' => $request->listing_id,
			'user_id' => $user_id,
		]);
    	return redirect()->route('cart.index')->with('success_msg', 'Property successfully added!');
    }

    public function destroy($id)
    {
    	Cart::whereId($id)->delete();
    	return redirect()->route('cart.index')->with('success_msg', 'Property successfully removed!');
    }
}