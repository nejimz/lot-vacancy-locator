<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\AccountActivation;

class RegistrationController extends Controller
{
    public function index()
    {
    	$email = old('email');
    	$is_broker = old('is_broker');

    	$first_name = old('first_name');
    	$last_name = old('last_name');
    	$mobile_number = old('mobile_number');
    	$phone_number = old('phone_number');

    	$agency_name = old('agency_name');
    	$agency_address = old('agency_address');
    	$agency_city = old('agency_city');

    	$data = compact('email', 'is_broker', 'first_name', 'last_name', 'mobile_number', 'phone_number', 'agency_name', 'agency_address', 'agency_city');

    	return view('auth.register', $data);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
        	'email' => 'required|email',
        	'password' => 'required',
        	'first_name' => 'required',
        	'last_name' => 'required',
        	'mobile_number' => 'required',
        	'agency_name' => 'required_if:is_broker,1',
        	'agency_address' => 'required_if:is_broker,1',
        	'agency_city' => 'required_if:is_broker,1'
        ], [
        	'agency_name.required_if' => 'The agency name field is required.',
        	'agency_address.required_if' => 'The agency address field is required.',
        	'agency_city.required_if' => 'The agency city field is required.'
        ]);

        $credentials = $request->only('email', 'password', 'first_name', 'last_name');
        $user = Sentinel::registerAndActivate($request->only('email', 'password', 'first_name', 'last_name'));
        $user->is_broker = $request->is_broker ?? 0;
        $user->mobile_number = $request->mobile_number;
        $user->phone_number = $request->phone_number;
        $user->agency_name = $request->agency_name;
        $user->agency_address = $request->agency_address;
        $user->agency_city = $request->agency_city;
        $user->save();
        #$activation = Activation::create($user);
        #Mail::to($request->email)->send(new AccountActivation());
        #dd($activation);
    	return redirect()->back()->with('success_msg', 'You successfuly created your account. We sent you an email for activation.');
    }
}
