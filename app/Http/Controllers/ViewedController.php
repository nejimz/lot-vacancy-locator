<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\AccessLog;
use App\Listing;
use App\ListingOwner;

class ViewedController extends Controller
{
    public function index()
    {
    	if (!Sentinel::check()) {
    		abort(404);
    	}

        $user = Sentinel::getUser();
        $rows = AccessLog::with('listing')->whereUserId($user->id)->orderBy('created_at', 'DESC')->paginate(50);
        
        $data = compact('rows');
        return view('listing.viewed', $data);
    }

    public function notification()
    {
        #ListingOwner::whereIn('listing.status', ['reserved', 'hold'])->where()->update(['seen_at'=>Carbon::now()]);

        $user_id = Sentinel::getUser()->id;
        $rows = ListingOwner::leftJoin('listing', 'listing_owners.listing_id', '=', 'listing.id')
                ->whereIn('listing.status', [0, 1])
                ->where('listing_owners.user_id', $user_id)
                ->paginate(50);
        #dd($rows);
        $data = compact('rows');
        return view('listing.notification', $data);
    }
}
