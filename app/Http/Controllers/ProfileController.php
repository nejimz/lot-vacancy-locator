<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Sentinel;
use Storage;

class ProfileController extends Controller
{
    public function index()
    {
    	$user = Sentinel::getUser();
    	#dd($user);
        $image = $user->image;
        $email = $user->email;
    	$is_broker = $user->is_broker;

    	$first_name = $user->first_name;
    	$last_name = $user->last_name;
    	$mobile_number = $user->mobile_number;
    	$phone_number = $user->phone_number;

    	$agency_name = $user->agency_name;
    	$agency_address = $user->agency_address;
    	$agency_city = $user->agency_city;
        
    	$data = compact('image', 'email', 'is_broker', 'first_name', 'last_name', 'mobile_number', 'phone_number', 'agency_name', 'agency_address', 'agency_city');

    	return view('account.profile', $data);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'profile_img' => 'image',
        	'first_name' => 'required',
        	'last_name' => 'required',
        	'mobile_number' => 'required',
        	'agency_name' => 'required_if:is_broker,1',
        	'agency_address' => 'required_if:is_broker,1',
        	'agency_city' => 'required_if:is_broker,1'
        ], [
            'profile_img.image' => 'The agency name field is required.',
            'agency_name.required_if' => 'The agency name field is required.',
        	'agency_address.required_if' => 'The agency address field is required.',
        	'agency_city.required_if' => 'The agency city field is required.'
        ]);
        $user = Sentinel::getUser();
        $file_location = '';
        if($request->has('profile_img')) {
            $user           = Sentinel::getUser();
            $file           = $request->profile_img;
            $extension      = $request->profile_img->getClientOriginalExtension();
            $file_name      = date('Ymdhis') . '_' . Str::random(10) . '.' . $extension;
            $file_location  = 'images/user/'.$user->id;
            
            if (Storage::exists($file_location)) {
                Storage::deleteDirectory($file_location);
            }
            Storage::makeDirectory($file_location);

            $request->profile_img->move(public_path($file_location), $file_name);
            $file_location .= '/'.$file_name;
        }

        $user = Sentinel::getUser();
        $user->mobile_number = $request->mobile_number;
        $user->phone_number = $request->phone_number;
        $user->agency_name = $request->agency_name;
        $user->agency_address = $request->agency_address;
        $user->agency_city = $request->agency_city;
        if($request->has('profile_img')) {
            $user->image = $file_location;
        }
        $user->save();
        #$activation = Activation::create($user);
        #Mail::to($request->email)->send(new AccountActivation());
        #dd($activation);
    	return redirect()->back()->with('success_msg', 'Profile successfuly updated.');
    }
}
