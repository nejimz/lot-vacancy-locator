<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class IsBroker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::getUser()->is_broker == 0) {
            abort(404);
        }
        return $next($request);
    }
}
