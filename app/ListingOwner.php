<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingOwner extends Model
{
    protected $connection = 'mysql';
    protected $table = 'listing_owners';
    protected $guarded = [];

    public function listing()
    {
    	return $this->hasMany('App\Listing', 'id', 'listing_id');
    }

    public function user()
    {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
