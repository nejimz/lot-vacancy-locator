<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ListingAction;
use App\ListingOwner;

class Listing extends Model
{
    protected $connection = 'mysql';
    protected $table = 'listing';
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function type()
    {
        return $this->hasOne('App\ListingType', 'id', 'listing_type_id');
    }

    public function image()
    {
        return $this->hasOne('App\ImageListing', 'id', 'listing_id');
    }

    public function images()
    {
        return $this->hasMany('App\ImageListing');
    }

    public function action()
    {
        return $this->hasMany('App\ListingAction', 'id', 'listing_id');
    }

    public function listing_owners()
    {
        #return $this->belongsTo('App\ListingOwner', 'id', 'listing_id');
        return ListingOwner::whereListingId($this->id)->get();
    }

    public function views()
    {
        $count = AccessLog::whereListingId($this->id)->count();
        return $count;
        #return $this->belongsTo('App\AccessLog', 'id', 'listing_id');
    }

    public function getListStatusAttribute()
    {
        if (is_null($this->status)) {
            return 'available';
        } elseif ($this->status == 0) {
            return 'hold';
        } elseif ($this->status == 1) {
            return 'reserved';
        }
    }

    public function view()
    {
        return $this->hasOne('App\AccessLog', 'id', 'listing_id');
    }

    public function listing_rating()
    {
        return $this->hasOne('App\ListingRating', 'id', 'listing_id');
    }

    public function getClientNameAttribute()
    {
        $action = ListingAction::with('client')->whereListingId($this->id)->whereUserId($this->user_id)->orderBy('created_at', 'DESC')->first();
        if (is_null($action)) {
            return '';
        } elseif(is_null($action->client)) {
            return '';
        }
        
        return $action->client->full_name;
    }
}