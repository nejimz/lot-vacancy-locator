<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingType extends Model
{
    protected $connection = 'mysql';
    protected $table = 'listing_type';
    protected $guarded = [];

    public function listing()
    {
    	return $this->belongsTo('App\Listing', 'id', 'listing_type_id');
    }
}
