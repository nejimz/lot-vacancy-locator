<?php

namespace App;

use Cartalyst\Sentinel\Users\EloquentUser;
use App\BrokerRating;

class User extends EloquentUser
{
    //use Notifiable;
    protected $connection = 'mysql';
    protected $table = 'users';
    protected $fillable = [
        'email', 'password', 'permissions', 'last_login', 
        'first_name', 'last_name', 'is_broker', 'mobile_number', 'phone_number', 
        'agency_name', 'agency_address', 'agency_city',
        'created_at', 'updated_at'
    ];
    protected $hidden = [
        'password',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function action()
    {
        return $this->belongsTo('App\ListingAction', 'id', 'client_id');
    }

    public function getRatingAttribute()
    {
        $count = BrokerRating::whereBrokerId($this->id)->count();
        $score = BrokerRating::whereBrokerId($this->id)->sum('rating');

        if ($count > 0) {
            $total_score = $score/$count;
            $star_rating = '';

            for($i = 1; $i <= $total_score; $i++) {
                $star_rating .= '<i class="fa fa-star-color fa-star"></i>';
            }

            for($n = 1; $n <= (5 - $total_score); $n++) {
                $star_rating .= '<i class="fa fa-star-color fa-star-o"></i>';
            }

            if(is_double($total_score)) {
                $star_rating .= '<i class="fa fa-star-color fa-star-half-empty"></i>';
            }

            return $star_rating;
        }
        return 0;
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
